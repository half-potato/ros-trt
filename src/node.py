import cv2
import rospy
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from .run_trt import TensorRTInfer
from .image_batcher import ImageBatcher
from PIL import Image
import sensor_msgs.msg.Image as ROSImage

import matplotlib.pyplot as plt
import argparse


def colorize(image, colormap='jet'):
    cmap = plt.get_cmap(colormap)
    norm = plt.Normalize(vmin=image.min(), vmax=np.percentile(image, 99.99))
    return cmap(norm(image))

class TRTNode:
    def __init__(self, engine: str, preprocessor: str):
        self.trt_infer = TensorRTInfer(engine)
        self.batcher = ImageBatcher(*self.trt_infer.input_spec(), preprocessor=preprocessor)
        self.bridge = CvBridge()

        self.color_pub = rospy.Publisher("seg/colorized", ROSImage)
        self.raw_pub = rospy.Publisher("seg/raw", ROSImage)

    def callback(self, img_msg):
        #  cv_img = self.bridge.imgmsg_to_cv2(img_msg, desired_encoding='passthrough')
        try:
            cv_img = self.bridge.imgmsg_to_cv2(img_msg, desired_encoding='rgb8')
        except CvBridgeError as e:
            print(e)
            return

        image = Image.fromarray(cv_img)
        batch = self.batcher.preprocess_image(image)
        output = self.trt_infer.infer(batch)
        recol = colorize(output.squeeze())

        try:
            self.color_pub.publish(self.bridge.cv2_to_imgmsg(recol, "rgb8"))
            self.raw_pub.publish(self.bridge.cv2_to_imgmsg(output, "32FC1"))
        except CvBridgeError as e:
            print(e)

def main(engine, preprocessor):
    node = TRTNode(engine, preprocessor)
    rospy.init_node('ros_trt', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
        cv2.destroyAllWindows()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--engine", help="The TensorRT engine to infer with")
    parser.add_argument("-p", "--preprocessor", default="CS",
                        help="Select the image preprocessor to use, either 'V2', 'V1' or 'V1MS', default: V1")
    args = parser.parse_args()

    main(args.engine, args.preprocessor)
